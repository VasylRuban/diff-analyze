# Diff analyser

Algorithm based on comparation attributes of different dom elements. 

Valuable attributes: textContent, rel, href, title, className.

Difference between content of attribute based on Levenshtein distance. 

# How to run

Install dependencies:

`npm i`

To run application, you can use suck command:

`node app.js ./samples/sample-0-origin.html ./samples/sample-1-evil-gemini.html`

Different Selector can be passed as the third arg. If it is not provided, make-everything-ok-button used by default.

`node app.js ./samples/sample-0-origin.html ./samples/sample-1-evil-gemini.html make-everything-ok-button`


Shortcuts for different test cases:

`npm run test-case-1` - first test case

`npm run test-case-2` - second test case

`npm run test-case-3` - third test case

`npm run test-case-4` - forth test case

# Output

Output will display path to similar dom element in such format:

`html > body > div > div > div.row > div.col-lg-8 > div.panel panel-default > div.panel-footer > a.btn btn-success`
