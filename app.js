const analyzer = require('./src/analyzer');

async function run() {
    let [ _, __, inputFile, diffFile, elementId ] = process.argv;

    elementId = elementId || 'make-everything-ok-button';

    const analyzeResult = await analyzer(inputFile, diffFile, elementId);

    process.stdout.write(analyzeResult + '\n');
}

run();
