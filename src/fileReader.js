const fs = require('fs');
const { JSDOM } = require('jsdom');

/**
 * Return dom of specified file
 *
 * @param path
 * @return {Promise<Object>}
 */
async function getFileContent(path) {
    const sampleFile = fs.readFileSync(path);
    const file = new JSDOM(sampleFile);

    return file.window.document;
}

module.exports = getFileContent;
