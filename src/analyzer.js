const levenshtein = require('fast-levenshtein');
const getFileContent = require('./fileReader');

const attributes = ['textContent', 'rel', 'href', 'title', 'className'];

/**
 * Search similar element
 *
 * @param originFile - path to source file
 * @param fileToCompare - path to file with some diff
 * @param elementId - Id of element from first file. The most similar element will be found in second file.
 * @return {Promise<void>}
 */
async function analyze(originFile, fileToCompare, elementId) {
    const [ originFileDOM, fileToCompareDOM ] = await Promise.all([
        getFileContent(originFile),
        getFileContent(fileToCompare)
    ]);

    const originalEl = originFileDOM.getElementById(elementId);
    const suspectedElements = fileToCompareDOM.querySelectorAll('*');

    let bestSolution;

    for (let i=0; i<suspectedElements.length; i++) {
        const suspectedEl = suspectedElements[i];

        const distances = attributes.map(item => compareByAttribute(originalEl, suspectedEl, item));
        const totalDistanceOfSuspectedEl = distances.reduce((acc, item) => acc + item, 0);

        bestSolution = getBestSolution(bestSolution, totalDistanceOfSuspectedEl, suspectedEl);

    }

    return getPath(bestSolution.element);

}

module.exports = analyze;

/**
 *  Get Levenstain distance between same attributes of different elements
 *
 * @param firstEl - element to compare
 * @param secondEl - element to compare
 * @param attr - attribute with this name will be compared
 * @return {*}
 */
function compareByAttribute(firstEl, secondEl, attr) {
    if (!secondEl[attr]) return firstEl[attr].length;

    return levenshtein.get(firstEl[attr], secondEl[attr]);
}

/**
 * Compare two solutions and returns best of them
 *
 * @param firstSolution - current best solution
 * @param secondSolutionScore - proposal solution score
 * @param secondSolutionEl - proposal solution element
 * @return {Object}
 */
function getBestSolution(firstSolution, secondSolutionScore, secondSolutionEl) {
    if (!firstSolution || firstSolution.score > secondSolutionScore) {
        return {
            score: secondSolutionScore,
            element: secondSolutionEl
        };
    }

    return firstSolution;
}

/**
 * Build path of dom element
 *
 * @param element
 * @return {string}
 */
function getPath(element) {
    let path = [];

    while(element) {
        let selector = element.tagName.toLowerCase();

        if (element.className) selector += '.' + element.className;

        path.push(selector);

        element = element.parentElement;
    }

    return path.reverse().join(' > ');
}
